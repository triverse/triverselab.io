---
title: "The States of Existence"
subtitle: "Triversal existence in closer look"
date: 2018-06-16
tags: ["open", "close", "existence", "social", "connection"]
---

There are three fundamental states of existence. From a triversal point of view, all existence is based on combinations of openness, closeness and bothness. Our solar system is full of variation of natures and biodiversity.

## Openness
It is our nature to be open as objects and individuals, part of solar systemic existence. We are all bound together in one gravitational force keeping us connected and relative. Every part is imcomplete without one another and require a delicate mutual dependency. In order to coexist and survive various conditions and environments, we all need each others involvement.

## Closeness
Every object or living being has a distinct state of existence that is there to sustain safety and security. Such state requires no social interaction with other objects or living beings.

## Bothness
The close and open state coexisting simulaneously is cryptic in its essence. The very social existence within nature or culture has always been both close and open at the same time. Every society, community or gathering evolved by sharing one or many languages, have been open for social interaction and connection while keeping individual safety intact.

