---
title: "The imperfect aliens"
subtitle: "Why we are perfect beings altogether."
date: 2018-06-07
tags: ["Imperfection", "Space", "Time", "Aliens", "Life"]
---

It is common conception that time and space require each other and that we eartlings are wondering about the aliens that might or might not be on our side of this universe we constantly try to fill with imagination and creativity. Our imperfect minds project perfection beyond actual existence.

## We are all aliens
Life on this planet didn't flourish without the external impact on biological existence. It is mere a projection that we all are local to this planet from the very beginning and the outside of this solar system is alien to us. It is mere projection that we are the center of existence and others are different and not part of our nature. The fact is that we all are aliens and part of a bigger existence than we can imagine. Every single being is part of space beyond our grasp. We share the same triverse and we share the same univeses.

## Space and Time
If you look from a triversal point of view, time does not exist and it is in fact an illusion. The closer we look into subject matter, that is, zoom in somewhere, the more time will be prevalent, yet not a dimension to be caunt on. Space on the other hand is relative, depending on where you are at certain point of existence, it will appear to be tangible. Without time there is actually no beginning or end of space, rather perpetual variation and movement. Inside triverse space is multi dimensional and time is a construction, measuring the movement of the space.

## Imperfection
Every single existence, life and matter is imperfect by nature. Without imperfection there can not be movement and change. Evolution tells the story of imperfect species adapting on imperect conditions. There is actually no real distinction between strong and weak, healthy and unhealthy, good and bad, since these are in essence imperfection. No being is better than another being and nothing last longer than anything else in this triverse. Our imperfection is palpable when we are isolated or excluded. Together we are closer to perfect existence and every single being and thing is significant part of this triversal perfection. Everyone and everything matters. This is why we need to embrace our imperfection and recognize our complete existence.
