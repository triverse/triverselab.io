---
title: About Triverse.Org
subtitle: The triverse of multitude.
comments: false
---

### Multiverse, Triverse, Universe and Verse explained

- Multiverse comprises more than one triverse.
- Triverse comprises more than one universe.
- Universe comprises one verse.
- Verse comprises more than one solar system.

### Multiverse
Multiverse is a field of transparent triverses. Triverses are spherical and attached to each other through multitude.

### Triverse
Triverse upholds universes that are close, open and both simultaneously. There are three kind of universes that propogate and populate trivese.

### Universe
Universe tend to withhold a singular verse. Thus de facto self-sufficient by essence.

### Verse
Verse is a field of transparent solar systems. Solar systems are spherical and part of galactic systems.

### Sunnyness
Sunnyness is the language that mediates the triverse of multitude.

### Gnulin Os
GNULIN is the operating system that harness the design philosophy utilized digitally.
