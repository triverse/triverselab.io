---
title: SunnynesS
subtitle: The language of triverse of multitude
comments: false
---

### SunnynesS script system
Sunnyness script intends to demonstrate multitude of expression in diverse manner. There are five ways of expressing sunnyness, and four of them are in written form. One can write from left to right or vice versa, or from top to bottom or vice versa. The fifth approach includes every direction simultaneously, which makes it multi dimensional expression. This last form is expressed in mind, picturing letters into combinations that can become words or sentences. Sunnyness is thus visualized inside your mind rather than written somehere exterior.

### SunnynesS alphabet license
The SunnynesS alphabet is carefully designed by Triverse.Org, which require that it remains intact without any modifications and thus consistently presented with the triversal language, which is a fundamental part of a philosophical approach that brings together coherent language of triversal existence. SunnynesS is intended to be used and shared by every living being in every aspect of social existence, as long as the above requirements are met.

### SunnynesS alphabet West-to-East
![SunnynesS alphabet West-to-East](/img/SunnynesS_alphabet_WE.png)
### SunnynesS alphabet East-to-West
![SunnynesS alphabet East-to-West](/img/SunnynesS_alphabet_EW.png)
### SunnynesS alphabet example
![SunnynesS alphabet example](/img/SunnynesS_alphabet_example.png)

