## Welcome to Triverse.Org

Triverse.Org intend to present comprehensive philosophical perspectives to mainstream world outlook in concurrent forms and perceptions. Currently, the main focus of Triverse.Org is on language development by practicing fundamental views of such philosophical standpoint. Triverse.Org doesn't however aim at analysing or informing on every aspects of life and social existence of societies.  

